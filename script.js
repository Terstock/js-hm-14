/* Теоритичні питання:
1. В чому полягає відмінність localStorage і sessionStorage? в localStorage дані зберігаються після закриття і відкриття нового браузера, а в sessionStorage після оновлення
2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage? шифрування даних, використання https протоколу, обмеження до даних, не зберігання паролів як таких
3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера? вони зникають, бо це не LocalStorage

Практичне завдання:

Реалізувати можливість зміни колірної теми користувача.

Технічні вимоги:

- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.

Примітки: 
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.

Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.
*/

let themeBtn = document.getElementById("themeBtn");

themeBtn.addEventListener("click", function () {
  let themeElem = localStorage.getItem("theme");
  toggleTheme(themeElem);
});

function toggleTheme(currentTheme) {
  if (currentTheme === "dark") {
    setTheme("light");
  } else {
    setTheme("dark");
  }
}

function setTheme(themeName) {
  localStorage.setItem("theme", themeName);
  document.body.className = themeName;
}

window.addEventListener("load", function () {
  let themeElem2 = localStorage.getItem("theme");
  setTheme(themeElem2);
});
